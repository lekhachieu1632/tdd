<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $table = 'tasks';

    protected $fillable = ['name', 'content'];

    public function scopeSearch($query)
    {
        if(request()->search)
        {
            $seacrh=request()->search;
            $query = $query->where('name', 'LIKE','%'.$seacrh.'%')->paginate(5);
        }

        return $query;
    }
}
