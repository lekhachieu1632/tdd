<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTaskRequest;
use App\Http\Requests\UpdateRequest;
use App\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TaskController extends Controller
{
    protected $task;

    /**
     * @param $task
     */
    public function __construct(Task $task){
        $this->task = $task;
    }

    public function index(){
        $tasks = $this->task->search()->latest('id')->paginate(10);
        return view('tasks.index', compact('tasks'));
    }

    public function show($id){
        $tasks = $this->task->findOrFail($id);
        return view('tasks.show', compact('tasks'));
    }

    public function store(CreateTaskRequest $request){
        $this->task->create($request->all());
        return redirect()->route('tasks.index');
    }

    public function create(){
        return view('tasks.create');
    }

    public function edit($id){
        $tasks = $this->task->findOrFail($id);
        return view('tasks.edit', compact('tasks'));
    }

    public function update(UpdateRequest $request, int $id){
        $tasks = $this->task->findOrFail($id);
        $tasks->update($request->all());
        return redirect()->route('tasks.index');
    }

    public function destroy($id)
    {
        $tasks = $this->task->findOrFail($id);
        $tasks->delete();
        return redirect()->route('tasks.index');
    }
}
