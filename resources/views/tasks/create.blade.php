@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2>Create task</h2>
            <form action="{{ route('tasks.store') }}" method="POST">
                @csrf
                <input id="name" type="text" name="name" placeholder="Name ...">
                @error('name')
                <div>{{ $message }}</div>
                @enderror
                <br/><input type="text" name="content" placeholder="Content ...">
                <br/><button type="submit">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
