@extends('layouts.app')

@section('content')
    <form action="" method="GET">
        <input type="text" name="search" placeholder="Search ...">
        <button>Sreach</button>
    </form>

    <br/>
    <br/>
    <br/>


    <table border="1" cellspacing="0" cellpadding="3">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Content</th>
            <th>Options</th>
        </tr>
        @foreach($tasks as $task)
            <tr>
                <td>{{ $task->id }}</td>
                <td>{{ $task->name }}</td>
                <td>{{ $task->content }}</td>
                <td>
                    <form action="{{ route('tasks.destroy', $task->id) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger">Delete</button>
                    </form>
                    <a href="{{ route('tasks.edit', $task->id) }}" class="btn btn-warning">Edit</a>
                    <a href="{{ route('tasks.show', $task->id) }}" class="btn btn-success">Detail</a>

                </td>
            </tr>
        @endforeach

    </table>
    {{ $tasks -> links('') }}
@endsection
