@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <form action="{{ route('tasks.update', $tasks->id) }}" method="POST">
                @csrf
                @method('PUT')
                <input id="name" type="text" name="name" value="{{ $tasks->name }}">
                @error('name')
                <div>{{ $message }}</div>
                @enderror
                <br/><input type="text" name="content" value="{{ $tasks->content }}">
                <br/>
                <button type="submit">Submit</button>
            </form>
        </div>
    </div>
@endsection
