@extends('layouts.app')

@section('content')
    <table border="1" cellspacing="0" cellpadding="3">
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Content</th>
        </tr>

        <tr>
            <td>{{ $tasks->id }}</td>
            <td>{{ $tasks->name }}</td>
            <td>{{ $tasks->content }}</td>
        </tr>
    </table>
    <a href="{{ route('tasks.index') }}" class="btn btn-success">Back</a>
@endsection
