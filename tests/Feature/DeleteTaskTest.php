<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class DeleteTaskTest extends TestCase
{
    public function getDeleteRoute($id)
    {
        return route('tasks.destroy', $id);
    }

    /** @test */
    public function authenticated_user_can_delete_task()
    {
        $this->actingAs(User::factory()->create());
        $tasks = Task::factory()->create();
        $response = $this->delete($this->getDeleteRoute($tasks->id));
        $this->assertDatabaseMissing('tasks', $tasks->toArray());
        $response->assertRedirect(route('tasks.index'));
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function unauthenticated_user_can_not_delete_task()
    {
        $task = Task::factory()->create();
        $response = $this->delete($this->getDeleteRoute($task->id));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }
}
