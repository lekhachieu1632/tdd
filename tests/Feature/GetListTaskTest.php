<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class GetListTaskTest extends TestCase
{
    public function getListTaskRoute()
    {
        return route('tasks.index');
    }

    public function getDetailTaskRoute($id)
    {
        return route('tasks.show', $id);
    }

    /** @test */
    public function authen_user_can_get_all_task()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getListTaskRoute());
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.index');
        $response->assertSee($task->name);
    }

    /** @test */
    public function user_can_get_detail_task()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getDetailTaskRoute($task->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertViewIs('tasks.show');
        $response->assertSee($task->name);
    }

}
