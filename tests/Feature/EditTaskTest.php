<?php

namespace Tests\Feature;

use App\Models\Task;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class EditTaskTest extends TestCase
{
    public function getEditRoute($id)
    {
        return route('tasks.edit', $id);
    }

    public function getUpdateRoute($id)
    {
        return route('tasks.update', $id);
    }

    /** @test */
    public function authenticated_user_can_view_edit_task()
    {
        $this->actingAs(User::factory()->create());
        $tasks = Task::factory()->create();
        $response = $this->get($this->getEditRoute($tasks->id));
        $response->assertViewIs('tasks.edit');
        $response->assertSee($tasks->name);
        $response->assertStatus(200);
    }

    /** @test */
    public function unauthenticated_user_can_not_view_edit_task()
    {
        $task = Task::factory()->create();
        $response = $this->get($this->getEditRoute($task->id));
        $response->assertRedirect('/login');
        $response->assertStatus(Response::HTTP_FOUND);
    }

    /** @test */
    public function authenticated_user_can_update_task()
    {
        $this->actingAs(User::factory()->create());
        $tasks = Task::factory()->create();
        $tasks['name'] = 'Name (update)';
        $response = $this->put($this->getUpdateRoute($tasks->id), $tasks->toArray());
        $this->assertDatabaseHas('tasks', ['id' => $tasks->id, 'name'=>'Name (update)']);
        $response->assertRedirect(route('tasks.index'));
        $response->assertStatus(302);
    }

    /** @test */
    public function unauthenticated_user_can_not_update_task()
    {
        $tasks = Task::factory()->create();
        $tasks['name'] = 'Name (update)';
        $response = $this->put($this->getUpdateRoute($tasks->id), $tasks->toArray());
        $response->assertRedirect(route('/login'));
        $response->assertStatus(302);
    }
}
